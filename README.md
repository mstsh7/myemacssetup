# MYEmacsSETUP
自分用に作ったemacs設定ファイル
## インストール方法
### 必須の操作
1. このリポジトリをクローンする  
`git clone https://gitlab.com/mstsh7/myemacssetup.git ~/.emacs.d`　
2. site-lisp以下にslime-repl-ansi-color.elを取得(Emacs内でslimeを使用するために必要)  
`mkdir site-lisp`  
`cd site-lisp`  
`wget https://raw.githubusercontent.com/deadtrickster/slime-repl-ansi-color/master/slime-repl-ansi-color.el`  
### ~~任意で行う~~
~~init.elのコメントアウトの内、60~101行(日本語入力に関するところ)は必要に応じてコメントアウトを外す~~  
必要なくなりました。
## パッケージ管理
Emacsで利用するパッケージの管理にはStraightとUse-packageを使用しています。使い方は以下を参照  
[straight.el](https://github.com/raxod502/straight.el)