;; -*-no-byte-compile: t; -*-

;;;; ----- Settings for Emacs26----- ;;;;

;;; Package manager
;; Straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Use-package
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

;;; load-path
(add-to-list 'load-path (expand-file-name "~/.emacs.d/site-lisp"))

;; Font
; default font
(set-face-attribute 'default nil :family "Cica" :height 120)
; proportional font
(set-face-attribute 'variable-pitch nil :family "Cica" :height 120)
; monospaced font
(set-face-attribute 'fixed-pitch nil :family "Cica" :height 120)
; tooltip font
(set-face-attribute 'tooltip nil :family "Cica" :height 90)

;; Color
(add-to-list 'default-frame-alist '(foreground-color . "#ffffff"))
(add-to-list 'default-frame-alist '(background-color . "#202040"))

;; Coding System
(prefer-coding-system 'utf-8-unix)
(setq default-process-coding-system '(undecided-dos . utf-8-unix))

;; Shell
(setq shell-file-name "/usr/bin/zsh")
(setq shell-command-switch "-c")
(setq explicit-shell-file-name shell-file-name)

;; migemo
;(use-package migemo)
;(setq migemo-command "cmigemo")
;(setq migemo-options '("-q" "--emacs"))
;(setq migemo-dictionary "/usr/share/cmigemo/utf-8/migemo-dict")
;(setq migemo-user-dictionary nil)
;(setq migemo-regex-dictionary nil)
;(setq migemo-coding-system 'utf-8-unix)
;(migemo-init)

(use-package mozc-popup)
(when (and (eq system-type 'gnu/linux)
           (getenv "WSLENV"))
  ;;; mozc
  (use-package mozc)
  (straight-use-package '(mozc-cursor-color
			              :type git :host github :repo "iRi-E/mozc-el-extensions"))
  (setq default-input-method "japanese-mozc")

  ;; enable popup style
  (setq mozc-candidate-style 'popup)

  ;; setting cursor color
  (setq mozc-cursor-color-alist '((direct . "red")
				                  (read-only . "yellow")
				                  (hiragana . "green")
				                  (full-katakana . "goldenrod")
				                  (half-ascii . "dark-orchid")
				                  (full-ascii . "orchid")
				                  (half-katakana . "dark goldenrod")))

  ;; toggle ime
  (global-set-key (kbd "C-o") 'toggle-input-method)
  (define-key isearch-mode-map (kbd "C-o")'isearch-toggle-input-mode)

  ;; mozc-cursor-color
  (defvar-local mozc-im-mode nil)
  (add-hook 'mozc-im-activate-hook (lambda () (setq mozc-im-mode t)))
  (add-hook 'mozc-im-deactivate-hook (lambda () (setq mozc-im-mode ni)))
  (advice-add 'mozc-cursor-color-update
	          :around (lambda (orig-fun &rest args)
		                (let ((mozc-mode mozc-im-mode))
			              (apply orig-fun args))))

  (add-hook 'isearch-mode-hook (lambda () (setq im-state mozc-im-mode)))
  (add-hook 'isearch-mode-end-hook
	        (lambda ()
	          (unless (eq im-state mozc-im-mode)
	            (if im-state
		            (activate-input-method default-input-method)
		          (deactivate-input-method))))))

;; disable cursor blink
(blink-cursor-mode 0)

;; wdired
(use-package wdired)
(advice-add 'wdired-finish-edit
	    :after (lambda (&rest args)
		     (deactivate-input-method)))

;;; auto-async-byte-compile
(use-package auto-async-byte-compile)
(add-hook 'emacs-lisp-mode-hook 'enable-auto-async-byte-compile-mode)

;;; frame size
(setq initial-frame-alist
          '((top . 60) (left . 35) (width . 80) (height . 30)))

;;; Disable auto-save & make-buckup
(setq make-backup-files nil)
(setq delete-auto-save-files t)
(setq backup-inhibited t)

;;; costomise tab mode
(setq-default tab-width 4 indent-tabs-mode nil)

;;; EOL
(setq eol-mnemonic-dos "(CRLF)")
(setq eol-mnemonic-mac "(CR)")
(setq eol-mnemonic-unix "(LF)")

(column-number-mode t)

(global-linum-mode t)

(show-paren-mode 1)

(setq scroll-conservatively 1)

(setq pc-select-selection-keys-only t)
(if (fboundp 'pc-selection-mode)
    (pc-selection-mode))

;;; y-or-n
(fset 'yes-or-no-p 'y-or-n-p)

;;; uniquify:display the dirctory name
(use-package uniquify
  :straight nil)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

;;; auto-complete
(straight-use-package '(auto-complete-config
                        :type git :host github :repo "auto-complete/auto-complete"))
(ac-config-default)

;;;; ----- Lisp ----- ;;;;
;;; slime
(add-to-list 'load-path (expand-file-name "~/.emacs.d/slime"))
;;(straight-use-package '(slime
;;			            :type git :host github :repo "deadtrickster/slime-repl-ansi-color"))
(use-package slime)
(setq inferior-lisp-program "ros run")
(slime-setup '(slime-repl slime-fancy slime-banner))
(slime-setup '(slime-fancy slime-repl-ansi-color))
(slime-setup)

(add-hook 'slime-mode-hook
	  '(lambda ()
	     (local-set-key (kbd "C-c s i") 'slime-restart-inferior-lisp)))

;;; ac-slime
(use-package ac-slime)
(add-hook 'slime-mode-hook 'set-up-slime-ac)
(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)

;;; paredit
(use-package paredit)
(eval-after-load "paredit"
  #'(define-key paredit-mode-map (kbd "C-c f") 'paredit-forward-slurp-sexp))
(eval-after-load "paredit"
  #'(define-key paredit-mode-map (kbd "C-c b") 'paredit-forward-barf-sexp))
(global-set-key (kbd "C-c m p") 'paredit-mode)

(add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
(add-hook 'slime-mode-hook 'enable-paredit-mode)
(add-hook 'slime-repl-mode-hook 'enable-paredit-mode)

;;; open-junk-file
(use-package open-junk-file)
(setq open-junk-file-format "~/.emacs.d/junk/%Y/%m/%Y-%m-%d-%H%M%S.")
(global-set-key (kbd "C-x z") 'open-junk-file)

;;; disable menu-bar & tool-bar
(tool-bar-mode 0)

;;; set window alpha
(defun set-alpha (alpha)
  (interactive "nAlpha: ")
  (set-frame-parameter nil 'alpha (cons alpha '(90))))
(global-set-key (kbd "C-x a") 'set-alpha)
